@spacl/yaml
======
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]
[![conventional commits][commits-image]][commits-url]

YAML parser and validator for [SPACL](https://www.npmjs.com/package/@spacl/core) policies.

## Installation
```
npm install @spacl/yaml
```
## Documentation
API documentation is available [here](https://cptpackrat.gitlab.io/spacl-yaml).

## Example
```yaml
# example.yml
version: 1
policies:
  # Create a policy describing a standard user who can
  # view other user's profiles, and edit their own.
  - name: user
    rules:
      - path: /user/+
        allow:
          - get
      - path: /user/:name
        allow:
          - put
  # Create a derived policy describing an admin user who
  # can also create, edit and delete any user's profile,
  # but for safety reasons, cannot delete themselves.
  - name: admin
    base: user
    rules:
      - path: /user/+
        allow:
          - put
          - post
          - delete
      - path: /user/:name
        deny:
          - delete
```
```js
import { parseFileSync } from '@spacl/yaml'

const policies = parseFileSync('example.yml')
const user = policies.get('user')
const admin = policies.get('admin')

/* Our hypothetical user, 'foo'. */
const ctx = {
  name: 'foo'
}

/* So, what happens if 'foo' is granted 'user' rights? */
user.query('/user/foo', 'get',    ctx) // true (explicitly allowed)
user.query('/user/foo', 'put',    ctx) // true (explicitly allowed)
user.query('/user/foo', 'delete', ctx) // null (implicitly denied)
user.query('/user/bar', 'get',    ctx) // true (explicitly allowed)
user.query('/user/bar', 'put',    ctx) // null (implicitly denied)
user.query('/user/bar', 'delete', ctx) // null (implicitly denied)

/* Alternatively, what if 'foo' is granted 'admin' rights? */
admin.query('/user/foo', 'get',    ctx) // true  (explicitly allowed)
admin.query('/user/foo', 'put',    ctx) // true  (explicitly allowed)
admin.query('/user/foo', 'delete', ctx) // false (explicitly denied)
admin.query('/user/bar', 'get',    ctx) // true  (explicitly allowed)
admin.query('/user/bar', 'put',    ctx) // true  (explicitly allowed)
admin.query('/user/bar', 'delete', ctx) // true  (explicitly allowed)
```

[npm-image]: https://img.shields.io/npm/v/@spacl/yaml.svg
[npm-url]: https://www.npmjs.com/package/@spacl/yaml
[pipeline-image]: https://gitlab.com/cptpackrat/spacl-yaml/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/spacl-yaml/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/spacl-yaml/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/spacl-yaml/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
[commits-image]: https://img.shields.io/badge/conventional%20commits-1.0.0-yellow.svg
[commits-url]: https://www.conventionalcommits.org
