
import { Policy } from '@spacl/core'
import { stringify as _stringify } from 'yaml'

/** Compile one or more policies into a policy document.
  * @param policies Policies to stringify. */
export function stringify (policies: Iterable<Policy>): string {
  const docs: object[] = []
  for (const policy of policies) {
    docs.push({
      name: policy.name,
      rules: policy.rules.map((rule) => {
        const verbs = Object.keys(rule.verbs)
        const allow = verbs.filter((verb) => rule.verbs[verb] === true)
        const deny = verbs.filter((verb) => rule.verbs[verb] === false)
        const doc: { [index: string]: any } = {
          path: rule.regex.spec
        }
        if (allow.length > 0) {
          doc.allow = allow.length > 1
            ? allow
            : allow[0]
        }
        if (deny.length > 0) {
          doc.deny = deny.length > 1
            ? deny
            : deny[0]
        }
        return doc
      })
    })
  }
  return _stringify({
    version: 1.2,
    policies: docs
  })
}
