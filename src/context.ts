
import { ParseOptions } from './parser'
import { ParseError } from './error'

export class ParseContext {
  opts: ParseOptions
  errs: ParseContextError[]
  src: string
  doc: any
  obj: any
  feat: {
    multiplePaths: boolean
    singleVerb: boolean,
    specVersion: '1.0' | '1.1'
  } = {
    multiplePaths: true,
    singleVerb: true,
    specVersion: '1.1'
  }

  constructor (base: ParseOptions | ParseContext, src: string, doc: any, obj: any = doc.toJSON()) {
    if (base instanceof ParseContext) {
      this.opts = base.opts
      this.errs = base.errs
      this.feat = base.feat
    } else {
      this.opts = base
      this.errs = []
    }
    this.src = src
    this.doc = doc
    this.obj = obj
  }

  try<T> (fn: () => T): T | undefined {
    try {
      return fn()
    } catch (err) {
      this.catch(err)
    }
  }

  catch (err: ParseContextError) {
    this.errs.push(err)
  }

  throw () {
    if (this.errs.length > 0) {
      throw new ParseError(this.src, ...this.errs)
    }
  }

  child (key: string | number): ParseContext {
    return new ParseContext(this, this.src, this.doc.get(key, true), this.obj[key])
  }

  blame (msg: string): never {
    throw new ParseContextError(this.doc, msg)
  }

  blameVal (key: string | number, msg: string): never {
    throw new ParseContextError(this.findBlameSite(key), msg)
  }

  blameKey (key: string | number, msg: string): never {
    throw new ParseContextError(this.findBlameSite(key, true), msg)
  }

  checkKeys (...keys: string[]) {
    for (const key of Object.keys(this.obj)) {
      if (!keys.includes(key)) {
        this.blameKey(key, `Unknown key '${key}'`)
      }
    }
  }

  checkVal (key: string, exp: string | string[], opt?: boolean) {
    if (key in this.obj) {
      const val = this.obj[key]
      const typ = Array.isArray(val) ? 'array' : typeof val
      if (Array.isArray(exp)) {
        if (!exp.includes(typ)) {
          this.blameVal(key, `Expected '${key}' to be one of [${exp.join(', ')}]`)
        }
      } else if (exp !== typ) {
        this.blameVal(key, `Expected '${key}' to be a ${exp}`)
      }
    } else if (!opt) {
      this.blame(`Missing required key '${key}'`)
    }
  }

  checkElem (key: string | number, exp: string) {
    const val = this.obj[key]
    const typ = typeof val
    if (exp === 'object') {
      if (Array.isArray(val) || val === null || typeof val !== 'object') {
        this.blameVal(key, `Expected ${exp} at index ${key}`)
      }
    } else if (typ !== exp) {
      this.blameVal(key, `Expected ${exp} at index ${key}`)
    }
  }

  findBlameSite (key: any, blameKey: boolean = false): any {
    let node = this.doc as any
    if (node.contents) {
      node = node.contents
    }
    const { type, items } = node
    if (type === 'SEQ' || type === 'FLOW_SEQ') {
      if (key in items && items[key] !== null) {
        return items[key]
      }
    } else {
      for (const item of items) {
        if (item && item.key && item.key.value === key) {
          return item.value && !blameKey
            ? item.value
            : item.key
        }
      }
    }
    return node
  }
}

export class ParseContextError extends Error {
  offset: number
  constructor (node: any, message: string) {
    super(message)
    this.offset = node.range
      ? node.range[0]
      : 0
  }
}
