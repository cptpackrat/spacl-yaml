
import { readFile, readFileSync } from 'fs'
import { Matcher, Rule, Policy, PolicyMap } from '@spacl/core'
import { ParseContext } from './context'
import { ParseError } from './error'

/* TODO: Remove type shortcuts if @types/yaml ever catches up. */
import * as yaml from 'yaml'

export interface ParseOptions {
  /** List of policy names that must be defined for parsing to succeed. */
  required?: Iterable<string>
  /** List of policies to use as a starting point; will be deep-cloned,
    * made available during parsing for inheritance, and returned as
    * part of the final results. */
  defaults?: Iterable<Policy>
  /** List of verbs that may be referenced by policies; all
    * verbs will be accepted if this option is omitted. */
  allowedVerbs?: string[]
  /** List of context properties that may be referenced by policies; no
    * context properties will be accepted if this option is omitted. */
  allowedProps?: string[]
  /** Verb to serve as a wildcard; occurences of this verb will be
    * with the contents of `allowedVerbs`, and as such this option
    * may only be used when `allowedVerbs` has also been provided. */
  wildcardVerb?: string
  /** Disable policy inheritance via the 'base' keyword. */
  disableInherit?: boolean
}

/** Parse a policy document.
  * @param src Source document.
  * @param opts Additional parser options. */
export function parse (src: string, opts: ParseOptions = {}): PolicyMap {
  const doc = yaml.parseDocument(src, {
    version: '1.2',
    schema: 'failsafe',
    merge: false,
    maxAliasCount: 0
  } as yaml.ParseOptions)
  if (doc.errors && doc.errors.length > 0) {
    throw new ParseError(src, ...doc.errors.map((err) => {
      return {
        message: err.message,
        offset: err.source.range
          ? err.source.range.start
          : 0
      }
    }))
  }
  if (!doc.contents) {
    throw new ParseError(src, {
      message: 'Empty document',
      offset: 0
    })
  }
  const map = new PolicyMap()
  if (opts.defaults) {
    for (const policy of opts.defaults) {
      map.set(policy.name, policy.clone())
    }
  }
  const ctx = new ParseContext(opts, src, doc)
  ctx.try(() => {
    parseDocument(ctx, map)
  })
  ctx.throw()
  return map
}

/** Read a file asynchronously and parse it as a policy document.
  * @param path Source document path.
  * @param opts Additional parser options. */
export function parseFile (path: string, opts?: ParseOptions): Promise<PolicyMap> {
  return new Promise((resolve, reject) => {
    return readFile(path, (err, buf) => {
      return err
        ? reject(err)
        : resolve(parse(buf.toString(), opts))
    })
  })
}

/** Read a file synchronously and parse it as a policy document.
  * @param path Source document path.
  * @param opts Additional parser options. */
export function parseFileSync (path: string, opts?: ParseOptions): PolicyMap {
  return parse(readFileSync(path).toString(), opts)
}

function parseDocument (ctx: ParseContext, map: Map<string, Policy>) {
  ctx.checkKeys('version', 'policies')
  ctx.checkVal('version', 'string', true)
  ctx.checkVal('policies', 'array')
  if ('version' in ctx.obj) {
    const allowed = ['1', '1.0', '1.1', '1.2']
    const version = ctx.obj.version
    if (!allowed.includes(version)) {
      ctx.blameVal('version', `Unsupported version '${version}'`)
    }
    if (version < 1.1) {
      ctx.feat.multiplePaths = false
    }
    if (version < 1.2) {
      ctx.feat.singleVerb = false
      ctx.feat.specVersion = '1.0'
    }
  }
  ctx = ctx.child('policies')
  for (const index in ctx.obj) {
    ctx.try(() => {
      ctx.checkElem(index, 'object')
      parsePolicy(ctx.child(index), map)
    })
  }
  if (ctx.opts.required) {
    for (const name of ctx.opts.required) {
      ctx.try(() => {
        if (!map.has(name)) {
          ctx.blame(`Missing definition for required policy '${name}'`)
        }
      })
    }
  }
}

function parsePolicy (ctx: ParseContext, map: Map<string, Policy>) {
  ctx.checkKeys('base', 'name', 'rules')
  ctx.checkVal('base', 'string', true)
  ctx.checkVal('name', 'string')
  ctx.checkVal('rules', 'array')
  const base = ctx.obj.base
    ? map.get(ctx.obj.base)
    : undefined
  if (ctx.obj.base) {
    if (ctx.opts.disableInherit) {
      ctx.blameKey('base', 'Inheritance is not permitted for this document')
    }
    if (!base) {
      ctx.blameVal('base', `Policy '${ctx.obj.base}' is not defined`)
    }
  }
  if (map.has(ctx.obj.name)) {
    ctx.blameVal('name', `Policy '${ctx.obj.name}' is already defined`)
  }
  const policy = base
    ? base.clone(ctx.obj.name)
    : new Policy(ctx.obj.name)
  ctx = ctx.child('rules')
  for (const index in ctx.obj) {
    ctx.try(() => {
      ctx.checkElem(index, 'object')
      parseRule(ctx.child(index), policy)
    })
  }
  map.set(policy.name, policy)
}

function parseRule (ctx: ParseContext, policy: Policy) {
  if (ctx.feat.multiplePaths) {
    ctx.checkKeys('path', 'paths', 'allow', 'deny')
    ctx.checkVal('path', 'string', true)
    ctx.checkVal('paths', 'array', true)
    if (!ctx.obj.path === !ctx.obj.paths) {
      ctx.blame('Expected exactly one of \'path\' or \'paths\'')
    }
  } else {
    ctx.checkKeys('path', 'allow', 'deny')
    ctx.checkVal('path', 'string')
  }
  ctx.checkVal('allow', ctx.feat.singleVerb ? ['array', 'string'] : 'array', true)
  ctx.checkVal('deny', ctx.feat.singleVerb ? ['array', 'string'] : 'array', true)
  const paths = ctx.try(() => ctx.obj.paths
    ? checkPaths(ctx.child('paths'), ctx.obj.paths)
    : [checkPath(ctx.child('path'), ctx.obj.path)])
  const allow = ctx.try(() => ctx.obj.allow
    ? Array.isArray(ctx.obj.allow)
      ? checkVerbs(ctx.child('allow'), ctx.obj.allow)
      : checkVerb(ctx.child('allow'), ctx.obj.allow)
    : [])
  const deny = ctx.try(() => ctx.obj.deny
    ? Array.isArray(ctx.obj.deny)
      ? checkVerbs(ctx.child('deny'), ctx.obj.deny)
      : checkVerb(ctx.child('deny'), ctx.obj.deny)
    : [])
  if (!ctx.obj.allow && !ctx.obj.deny) {
    ctx.blame('Expected at least one of \'allow\' or \'deny\'')
  }
  if (paths && allow && deny) {
    paths.forEach((path) => {
      for (const rule of policy.rules) {
        if (rule.regex.spec === path.spec) {
          rule
            .allow(...allow)
            .deny(...deny)
          return
        }
      }
      policy.push(Rule
        .for(path)
        .allow(...allow)
        .deny(...deny))
    })
  }
}

function checkPath (ctx: ParseContext, path: string): Matcher {
  const allowed = ctx.opts.allowedProps
  let matcher
  try {
    matcher = Matcher.for(path, ctx.feat.specVersion)
  } catch (err) {
    throw ctx.blame(err.message)
  }
  if (allowed) {
    for (const prop of matcher.props) {
      if (!allowed.includes(prop)) {
        ctx.blame(`Unknown context property '${prop}', expected one of [${allowed.join(', ')}]`)
      }
    }
  } else if (matcher.props.length > 1) {
    ctx.blame('Context properties are not permitted for this document')
  }
  return matcher
}

function checkPaths (ctx: ParseContext, paths: string[]): Matcher[] {
  if (paths.length < 1) {
    ctx.blame('Path list must not be empty')
  }
  return paths.map((path, index) => {
    ctx.checkElem(index, 'string')
    return checkPath(ctx.child(index), path)
  })
}

function checkVerb (ctx: ParseContext, verb: string): string[] {
  const wildcard = ctx.opts.wildcardVerb
  const allowed = ctx.opts.allowedVerbs
  if (allowed) {
    if (verb === wildcard) {
      return allowed
    } else if (!allowed.includes(verb)) {
      const list = wildcard
        ? allowed.concat(wildcard).join(', ')
        : allowed.join(', ')
      ctx.blame(`Unknown verb '${verb}', expected one of [${list}]`)
    }
  }
  return [verb]
}

function checkVerbs (ctx: ParseContext, verbs: any[]): string[] {
  if (verbs.length < 1) {
    ctx.blame('Verb lists must not be empty')
  }
  const wildcard = ctx.opts.wildcardVerb
  const allowed = ctx.opts.allowedVerbs
  for (const index in verbs) {
    ctx.checkElem(index, 'string')
    const verb = ctx.obj[index]
    if (allowed) {
      if (verb === wildcard) {
        verbs = allowed
      } else if (!allowed.includes(verb)) {
        const list = wildcard
          ? allowed.concat(wildcard).join(', ')
          : allowed.join(', ')
        ctx.blameVal(index, `Unknown verb '${verb}', expected one of [${list}]`)
      }
    }
  }
  return verbs
}
