
export class ParseError extends Error {
  /** Source document. */
  readonly source: string
  /** Individual error descriptions. */
  readonly details: {
    /** Error message. */
    message: string
    /** Error line number. */
    line: number
    /** Error column number. */
    column: number
    /** Error byte offset. */
    offset: number
  }[]

  constructor (source: string, ...errors: {
    message: string
    offset: number
  }[]) {
    const lengths = source.split('\n').map((line) => line.length + 1)
    const details = errors.map((error) => {
      let offset = error.offset
      let number = 1
      for (const length of lengths) {
        if (offset < length) {
          break
        }
        offset -= length
        number++
      }
      return {
        message: error.message,
        offset: error.offset,
        line: number,
        column: offset
      }
    })
    const { line, column, message } = details[0]
    super(details.length > 1
      ? `line ${line}:${column}: ${message} (+ ${details.length - 1} more)`
      : `line ${line}:${column}: ${message}`)
    this.source = source
    this.details = details
  }

  /** Returns a full description of one or all of error messages.
    * @param index Specific error to describe; if omitted, all errors
    *              will be joined into a newline-delimited string. */
  describe (index?: number): string {
    if (index === undefined) {
      return this.details.map((_, index) => this.describe(index)).join('\n')
    }
    const { line, column, message } = this.details[index]
    return `line ${line}:${column}: ${message}`
  }

  /** Returns an annotated copy of the source document highlighting errors.
    * @param noColour Do not use terminal colours. */
  annotate (noColours: boolean = false): string {
    return `${this._annotate(0, noColours)}\n${
      noColours ? '' : '\u001b[31m'}${
      this.details.map((detail, index) => {
        return `[${index + 1}] ${detail.message}`
      }).join('\n')}${
      noColours ? '' : '\u001b[0m'}`
  }

  private _annotate (index: number, noColours: boolean): string {
    const source = index < this.details.length - 1
      ? this._annotate(index + 1, noColours)
      : this.source
    return `${
      source.slice(0, this.details[index].offset)}${
      noColours ? '' : '\u001b[31m'}[${index + 1}] ${
      noColours ? '' : '\u001b[0m'}${
      source.slice(this.details[index].offset)}`
  }
}
