/* eslint-disable no-multi-spaces */

import * as assert from 'assert'
import { Rule, Policy, PolicyMap } from '@spacl/core'
import { stringify } from '../src/stringify'
import { ParseError } from '../src/error'
import {
  parse,
  parseFile,
  parseFileSync,
  ParseOptions
} from '../src'

describe('parse', () => {
  context('during basic validation', () => {
    it('rejects empty documents', () => {
      expectFail('')
    })
    it('rejects invalid yaml', () => {
      expectFail(`foo
          : bar
      `, /single.+line/,
      /bad.+indentation/)
    })
    it('rejects non-array \'policies\'', () => {
      expectFail('policies:')
      expectFail('policies: foo')
      expectFail('policies: {}')
    })
    it('accepts empty \'policies\'', () => {
      expectPass('policies: []')
    })
    it('accepts optional \'version\'', () => {
      const doc = `
        policies: []
        version: `
      expectPass(`${doc} 1`)
      expectPass(`${doc} 1.0`)
      expectPass(`${doc} 1.1`)
      expectPass(`${doc} 1.2`)
    })
    it('rejects non-string \'version\'', () => {
      const doc = `
        policies: []
        version: `
      expectFail(`${doc}`,    /version.+string/i)
      expectFail(`${doc} []`, /version.+string/i)
      expectFail(`${doc} {}`, /version.+string/i)
    })
    it('rejects unknown \'version\'', () => {
      expectFail(`
        version: 2
        policies: []
      `, /unsupported.+version/i)
    })
    it('rejects unknown keys', () => {
      expectFail('foo: bar', /unknown.+foo/i)
    })
    context('during validation of \'policies\'', () => {
      it('rejects non-objects', () => {
        const doc = `
          policies:
            - `
        expectFail(`${doc}`,     /expected.+object/i)
        expectFail(`${doc} []`,  /expected.+object/i)
        expectFail(`${doc} foo`, /expected.+object/i)
      })
      it('rejects missing \'name\'', () => {
        expectFail(`
          policies:
            - {}
        `, /missing.+name/i)
      })
      it('rejects non-string \'name\'', () => {
        const doc = `
          policies:
            - name: `
        expectFail(`${doc}`,    /name.+string/i)
        expectFail(`${doc} []`, /name.+string/i)
        expectFail(`${doc} {}`, /name.+string/i)
      })
      it('rejects duplicate \'name\'', () => {
        expectFail(`
          policies:
            - name: foo
              rules: []
            - name: foo
              rules: []
        `, /foo.+defined/i)
      })
      it('rejects non-string \'base\'', () => {
        const doc = `
          policies:
            - base: `
        expectFail(`${doc}`,    /base.+string/i)
        expectFail(`${doc} []`, /base.+string/i)
        expectFail(`${doc} {}`, /base.+string/i)
      })
      it('accepts known \'base\' policy', () => {
        expectPass(`
          policies:
            - name: foo
              rules: []
            - base: foo
              name: bar
              rules: []
        `)
      })
      it('rejects unknown \'base\' policy', () => {
        expectFail(`
          policies:
            - base: foo
              name: bar
              rules: []
        `, /foo.+not.+defined/i)
      })
      it('rejects missing \'rules\'', () => {
        expectFail(`
          policies:
            - name: foo
        `, /missing.+rules/i)
      })
      it('rejects non-array \'rules\'', () => {
        const doc = `
          policies:
            - name: foo
              rules: `
        expectFail(`${doc}`,     /rules.+array/i)
        expectFail(`${doc} {}`,  /rules.+array/i)
        expectFail(`${doc} foo`, /rules.+array/i)
      })
      it('accepts empty \'rules\'', () => {
        expectPass(`
          policies:
            - name: foo
              rules: []
        `)
      })
      it('rejects unknown keys', () => {
        expectFail(`
          policies:
            - foo: bar
        `, /unknown.+foo/i)
      })
    })
    context('during validation of \'rules\'', () => {
      it('rejects non-objects', () => {
        const doc = `
          policies:
            - name: foo
              rules:
                - `
        expectFail(`${doc}`,     /expected.+object/i)
        expectFail(`${doc} []`,  /expected.+object/i)
        expectFail(`${doc} foo`, /expected.+object/i)
      })
      it('rejects none of \'path\' and \'paths\'', () => {
        expectFail(`
          policies:
          - name: foo
            rules:
              - {}
        `, /exactly.+one.+path.+paths/i)
      })
      it('rejects both of \'path\' and \'paths\'', () => {
        expectFail(`
          policies:
          - name: foo
            rules:
              - path: /foo
                paths:
                  - /bar
        `, /exactly.+one.+path.+paths/i)
      })
      it('rejects non-string \'path\'', () => {
        const doc = `
          policies:
          - name: foo
            rules:
              - path: `
        expectFail(`${doc}`,    /path.+string/i)
        expectFail(`${doc} []`, /path.+string/i)
        expectFail(`${doc} {}`, /path.+string/i)
      })
      it('rejects invalid \'path\'', () => {
        const doc = `
          policies:
          - name: foo
            rules:
              - path: `
        expectFail(`${doc} foo`, /path.+begin.+slash/i)
        expectFail(`${doc} //`,  /path.+empty.+segment/i)
      })
      it('rejects non-array \'paths\'', () => {
        const doc = `
          policies:
            - name: foo
              rules:
                - paths: `
        expectFail(`${doc}`,     /paths.+array/i)
        expectFail(`${doc} {}`,  /paths.+array/i)
        expectFail(`${doc} foo`, /paths.+array/i)
      })
      it('rejects empty \'paths\'', () => {
        expectFail(`
          policies:
            - name: foo
              rules:
                - paths: []
          `, /path.+empty/i)
      })
      it('rejects none of \'allow\' and \'deny\'', () => {
        expectFail(`
          policies:
          - name: foo
            rules:
              - path: /foo
        `, /one.+of.+allow.+deny/i)
      })
      it('rejects non-array/string \'allow\'', () => {
        const doc = `
          policies:
            - name: foo
              rules:
                - path: /foo
                  allow: `
        expectFail(`${doc}`,     /allow.+array/i)
        expectFail(`${doc} {}`,  /allow.+array/i)
      })
      it('rejects empty \'allow\'', () => {
        expectFail(`
          policies:
            - name: foo
              rules:
                - path: /foo
                  allow: []
          `, /verb.+empty/i)
      })
      it('rejects non-array/string \'deny\'', () => {
        const doc = `
          policies:
            - name: foo
              rules:
                - path: /foo
                  deny: `
        expectFail(`${doc}`,     /deny.+array/i)
        expectFail(`${doc} {}`,  /deny.+array/i)
      })
      it('rejects empty \'deny\'', () => {
        expectFail(`
          policies:
            - name: foo
              rules:
                - path: /foo
                  deny: []
          `, /verb.+empty/i)
      })
      it('rejects unknown keys', () => {
        expectFail(`
          policies:
            - name: foo
              rules:
                - foo: bar
        `, /unknown.+foo/i)
      })
    })
    context('during validation of \'paths\'', () => {
      it('rejects non-strings', () => {
        const doc = `
          policies:
            - name: foo
              rules:
                - paths:
                    - `
        expectFail(`${doc}`,    /expected.+string/i)
        expectFail(`${doc} []`, /expected.+string/i)
        expectFail(`${doc} {}`, /expected.+string/i)
      })
      it('rejects invalid paths', () => {
        const doc = `
          policies:
          - name: foo
            rules:
              - paths:
                  - `
        expectFail(`${doc} foo`, /path.+begin.+slash/i)
        expectFail(`${doc} //`,  /path.+empty.+segment/i)
      })
    })
    context('during validation of \'allow\'', () => {
      it('rejects non-strings', () => {
        const doc = `
          policies:
            - name: foo
              rules:
                - path: /foo
                  allow:
                    - `
        expectFail(`${doc}`,    /expected.+string/i)
        expectFail(`${doc} []`, /expected.+string/i)
        expectFail(`${doc} {}`, /expected.+string/i)
      })
    })
    context('during validation of \'deny\'', () => {
      it('rejects non-strings', () => {
        const doc = `
          policies:
            - name: foo
              rules:
                - path: /foo
                  deny:
                    - `
        expectFail(`${doc}`,    /expected.+string/i)
        expectFail(`${doc} []`, /expected.+string/i)
        expectFail(`${doc} {}`, /expected.+string/i)
      })
    })
  })
  context('when using \'required\'', () => {
    const opts = {
      required: ['foo', 'bar']
    }
    it('accepts documents that satisfy list', () => {
      expectPass(`
        policies:
          - name: foo
            rules: []
          - name: bar
            rules: []
      `, opts)
    })
    it('rejects documents that don\'t', () => {
      expectFail(`
        policies: []
      `, opts,
      /missing.+policy.+foo/i,
      /missing.+policy.+bar/i)
    })
  })
  context('when using \'defaults\'', () => {
    const opts = {
      defaults: [
        Policy.for('foo',
          Rule.for('/foo').allow('foo')),
        Policy.for('bar',
          Rule.for('/bar').allow('bar'))
      ]
    }
    it('clones \'defaults\' into results', () => {
      const map = expectPass(`
        policies: []
      `, opts)
      const foo = map.get('foo')
      const bar = map.get('bar')
      assert(foo)
      assert(bar)
      if (foo && bar) {
        assert(foo.query('/foo', 'foo'))
        assert(bar.query('/bar', 'bar'))
        assert.notStrictEqual(foo, opts.defaults[0])
        assert.notStrictEqual(bar, opts.defaults[1])
      }
    })
    it('makes \'defaults\' available for inheritance', () => {
      const map = expectPass(`
        policies:
          - base: foo
            name: boo
            rules:
              - path: /foo
                allow:
                  - boo
      `, opts)
      const foo = map.get('foo')
      const boo = map.get('boo')
      assert(foo)
      assert(boo)
      if (foo && boo) {
        assert(foo.query('/foo', 'foo'))
        assert(boo.query('/foo', 'boo'))
        assert.notStrictEqual(foo, boo)
      }
    })
  })
  context('when using \'allowedVerbs\'', () => {
    const opts = {
      allowedVerbs: ['foo', 'bar']
    }
    it('accepts known verbs', () => {
      expectPass(`
        policies:
          - name: foo
            rules:
              - path: /foo
                allow:
                  - foo
                deny:
                  - bar
      `, opts)
    })
    it('rejects unknown verbs', () => {
      expectFail(`
        policies:
          - name: foo
            rules:
              - path: /
                allow:
                  - boo
                deny:
                  - baz
              - path: /
                allow: hurf
                deny: durf
      `, opts,
      /verb.+boo.+expected.+foo.+bar/i,
      /verb.+baz.+expected.+foo.+bar/i,
      /verb.+hurf.+expected.+foo.+bar/i,
      /verb.+durf.+expected.+foo.+bar/i)
    })
    context('otherwise', () => {
      it('accepts any verb', () => {
        expectPass(`
          policies:
            - name: foo
              rules:
                - path: /foo
                  allow:
                    - foo
                    - bar
        `)
      })
    })
  })
  context('when using \'allowedProps\'', () => {
    const opts = {
      allowedProps: ['foo', 'bar']
    }
    it('accepts known context properties', () => {
      expectPass(`
        policies:
          - name: foo
            rules:
              - path: /:foo/:bar
                allow:
                  - foo
      `, opts)
    })
    it('rejects unknown context properties', () => {
      expectFail(`
        policies:
          - name: foo
            rules:
              - path: /:boo
                allow:
                  - foo
              - path: /:baz
                deny:
                  - foo

      `, opts,
      /property.+boo.+expected.+foo.+bar/i,
      /property.+baz.+expected.+foo.+bar/i)
    })
    context('otherwise', () => {
      it('rejects all context properties', () => {
        expectFail(`
          policies:
            - name: foo
              rules:
                - path: /:foo/:bar
        `, /properties.+not.+permitted/i)
      })
    })
  })
  context('when using \'wildcardVerb\'', () => {
    it('replaces wildcard with contents of \'allowedVerbs\'', () => {
      const foo = expectPass(`
        policies:
          - name: foo
            rules:
              - path: /
                deny:
                  - all
      `, {
        allowedVerbs: ['foo', 'bar'],
        wildcardVerb: 'all'
      }).get('foo')
      assert(foo)
      if (foo) {
        assert(foo.query('/', 'foo') === false)
        assert(foo.query('/', 'bar') === false)
        assert(foo.query('/', 'all') === null)
      }
    })
    it('ignores wildcard if \'allowedVerbs\' is not set', () => {
      const set = expectPass(`
        policies:
          - name: foo
            rules:
              - path: /
                deny:
                  - all
          - name: bar
            rules:
              - path: /
                deny: all
      `, {
        wildcardVerb: 'all'
      })
      const foo = set.get('foo')
      const bar = set.get('bar')
      assert(foo)
      assert(bar)
      if (foo) {
        assert(foo.query('/', 'all') === false)
      }
      if (bar) {
        assert(bar.query('/', 'all') === false)
      }
    })
    it('includes wildcard in error reporting', () => {
      expectFail(`
        policies:
          - name: boo
            rules:
              - path: /foo
                allow:
                  - nope
                deny: also
      `, {
        allowedVerbs: ['foo', 'bar'],
        wildcardVerb: 'all'
      }, /verb.+nope.+foo.+bar.+all/i,
      /verb.+also.+foo.+bar.+all/i)
    })
  })
  context('when using \'disableInherit\'', () => {
    it('inheritance is prohibited', () => {
      expectFail(`
        policies:
          - name: foo
            rules: []
          - base: foo
            name: bar
            rules: []
      `, {
        disableInherit: true
      }, /inheritance.+not.+permitted/i)
    })
  })
  context('when parsing document versions below \'1.1\'', () => {
    it('rejects use of \'paths\'', () => {
      const doc = `
        policies:
          - name: foo
            rules:
              - paths:
                  - /foo
                  - /bar
        version: `
      expectFail(`${doc} 1`,   /unknown.+paths/i)
      expectFail(`${doc} 1.0`, /unknown.+paths/i)
    })
    it('rejects missing \'path\'', () => {
      const doc = `
        policies:
          - name: foo
            rules:
              - {}
        version: `
      expectFail(`${doc} 1`,   /missing.+path/i)
      expectFail(`${doc} 1.0`, /missing.+path/i)
    })
  })
  context('when parsing document versions below \'1.2\'', () => {
    it('rejects single-string \'allow\'', () => {
      const doc = `
        policies:
          - name: foo
            rules:
              - path: /foo
                allow: bar
        version: `
      expectFail(`${doc} 1`,   /allow.+array/i)
      expectFail(`${doc} 1.0`, /allow.+array/i)
      expectFail(`${doc} 1.1`, /allow.+array/i)
    })
    it('rejects single-string \'deny\'', () => {
      const doc = `
        policies:
          - name: foo
            rules:
              - path: /foo
                deny: bar
        version: `
      expectFail(`${doc} 1`,   /deny.+array/i)
      expectFail(`${doc} 1.0`, /deny.+array/i)
      expectFail(`${doc} 1.1`, /deny.+array/i)
    })
    it('enforces path specification v1 features', () => {
      const doc = `
        policies:
          - name: foo
            rules:
              - path: /**
              - path: /++
        version: `
      expectFail(`${doc} 1`,   /malformed.+wildcard/i)
      expectFail(`${doc} 1.0`, /malformed.+wildcard/i)
      expectFail(`${doc} 1.1`, /malformed.+wildcard/i)
    })
  })
})

const opts = {
  allowedVerbs: ['read', 'write', 'create', 'delete'],
  wildcardVerb: 'all',
  defaults: [
    Policy.for('foo',
      Rule.for('/foo').allow('read')),
    Policy.for('bar',
      Rule.for('/bar').allow('read'))
  ]
}

const exp = new PolicyMap(
  Policy.for('foo',
    Rule.for('/foo').allow('read')),
  Policy.for('foo_admin',
    Rule.for('/foo').allow('read', 'write', 'create', 'delete')),
  Policy.for('bar',
    Rule.for('/bar').allow('read')),
  Policy.for('bar_editor',
    Rule.for('/bar').allow('read', 'write')),
  Policy.for('boo',
    Rule.for('/boo').allow('read', 'write', 'create').deny('delete'),
    Rule.for('/baz').allow('read', 'write', 'create', 'delete')))

describe('parseFile', () => {
  it('reads files asynchronously', async () => {
    assert.deepStrictEqual(await parseFile('test/test.yml', opts), exp)
  })
  it('rejects readFile errors', async () => {
    await assert.rejects(() => parseFile('test/nope.yml'))
  })
})

describe('parseFileSync', () => {
  it('reads files synchronously', () => {
    assert.deepStrictEqual(parseFileSync('test/test.yml', opts), exp)
  })
  it('throws readFileSync errors', () => {
    assert.throws(() => parseFileSync('test/nope.yml'))
  })
})

describe('error', () => {
  let err: ParseError
  before(() => {
    err = expectFail(`
      policies:
        - oops
        - name: hmmm
        - name: foo
          rules:
            - path: /oh/no
    `, /expected.+object/i,
    /missing.+rules/i,
    /expected.+allow.+deny/i)
  })
  it('uses the first error as the primary message', () => {
    assert.strictEqual(err.message, 'line 3:10: Expected object at index 0 (+ 2 more)')
  })
  it('describes individual errors', () => {
    assert.strictEqual(err.describe(0), 'line 3:10: Expected object at index 0')
    assert.strictEqual(err.describe(1), 'line 4:10: Missing required key \'rules\'')
    assert.strictEqual(err.describe(2), 'line 7:14: Expected at least one of \'allow\' or \'deny\'')
  })
  it('describes all errors', () => {
    assert.strictEqual(err.describe(), [
      'line 3:10: Expected object at index 0',
      'line 4:10: Missing required key \'rules\'',
      'line 7:14: Expected at least one of \'allow\' or \'deny\''
    ].join('\n'))
  })
  it('annotates error locations', () => {
    assert.strictEqual(err.annotate(true), [`
      policies:
        - [1] oops
        - [2] name: hmmm
        - name: foo
          rules:
            - [3] path: /oh/no
    `,  '[1] Expected object at index 0',
    '[2] Missing required key \'rules\'',
    '[3] Expected at least one of \'allow\' or \'deny\''
    ].join('\n'))
  })
  it('annotates error locations with colour', () => {
    assert.strictEqual(err.annotate(), [`
      policies:
        - \u001b[31m[1] \u001b[0moops
        - \u001b[31m[2] \u001b[0mname: hmmm
        - name: foo
          rules:
            - \u001b[31m[3] \u001b[0mpath: /oh/no
    `,  '\u001b[31m[1] Expected object at index 0',
    '[2] Missing required key \'rules\'',
    '[3] Expected at least one of \'allow\' or \'deny\'\u001b[0m'
    ].join('\n'))
  })
})

describe('stringify', () => {
  const policies = [
    Policy.for('foo',
      Rule.for('/foo').allow('foo').deny('bar'),
      Rule.for('/bar').deny('foo', 'bar', 'boo')),
    Policy.for('bar',
      Rule.for('/bar').allow('bar', 'boo'),
      Rule.for('/foo').deny('foo', 'bar', 'boo'))
  ]
  it('returns valid policy documents', () => {
    assert.deepStrictEqual(comparify(stringify(policies)), comparify(`
      version: 1.2
      policies:
        - name: foo
          rules:
            - path: /foo
              allow: foo
              deny: bar
            - path: /bar
              deny:
                - foo
                - bar
                - boo
        - name: bar
          rules:
            - path: /bar
              allow:
                - bar
                - boo
            - path: /foo
              deny:
                - foo
                - bar
                - boo
    `))
  })
  it('can perform round-trips with parse', () => {
    assert.deepStrictEqual(parse(stringify(policies)), new PolicyMap(...policies))
  })
})

function expectPass (doc: string, opts?: ParseOptions) {
  try {
    return parse(doc, opts)
  } catch (err) {
    const msg = err.annotate
      ? err.annotate(true)
      : err.toString
        ? err.toString()
        : err.message
    throw new Error(`Failed unexpectedly.\nOutput: {\n${msg}\n}`)
  }
}

function expectFail (doc: string, opts?: ParseOptions | RegExp, ...pats: RegExp[]) {
  if (opts instanceof RegExp) {
    pats = [opts].concat(pats)
    opts = undefined
  }
  const exp = pats.join('\n')
  try {
    parse(doc, opts as any)
  } catch (err) {
    const msg = err.annotate
      ? err.annotate(true)
      : err.toString
        ? err.toString()
        : err.message
    if (err.details && pats.every((pat) => err.details.some((err: any) => err.message.match(pat)))) {
      return err
    }
    throw new Error(`Failed with unexpected errors.\nOutput: {\n${msg}\n}\nExpected: [\n${exp}\n]`)
  }
  throw new Error(`Succeeded unexpectedly.\nExpected: [\n${exp}\n]`)
}

function comparify (src: string) {
  const lines = src.split('\n').filter((line) => line.match(/\S+/))
  const space = lines.reduce((min, line) => {
    const match = line.match(/^\s+/)
    return Math.min(min, match ? match[0].length : 0)
  }, Infinity)
  return lines.map((line) => line.slice(space))
}
