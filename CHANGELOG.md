# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.3.5](https://gitlab.com/cptpackrat/spacl-yaml/compare/v1.3.4...v1.3.5) (2020-04-02)


### Housekeeping

* Updated dependencies. ([51098fc](https://gitlab.com/cptpackrat/spacl-yaml/commit/51098fc4a7bd513fbb2916d0704e2c9a7102ad66))


### Improvements

* Switched to returning PolicyMap from parse functions. ([7702812](https://gitlab.com/cptpackrat/spacl-yaml/commit/7702812255357f3585fd5f1811465dfe7e42897c))

## [1.3.4](https://gitlab.com/cptpackrat/spacl-yaml/compare/v1.3.3...v1.3.4) (2020-03-29)


### Housekeeping

* Added CI step to publish on release tags. ([c5864aa](https://gitlab.com/cptpackrat/spacl-yaml/commit/c5864aa004c92a28137c38b38e50d9677977d286))
* Fixed CI audit failing on dev dependencies; switched to using 'npm ci'. ([7b5aca3](https://gitlab.com/cptpackrat/spacl-yaml/commit/7b5aca3b59243a30361597eab4cb480c7cd7909f))
* Fixed linter missing files; renamed lint script to 'lint'. ([313e8d6](https://gitlab.com/cptpackrat/spacl-yaml/commit/313e8d6701a13e6a8dad04ccbf13cb8449615508))
* Fixed npm complaining about permissions in CI. ([65a8e6d](https://gitlab.com/cptpackrat/spacl-yaml/commit/65a8e6df47f147af9121975f965aca742be9354a))
* Updated dependencies. ([6d3a9d5](https://gitlab.com/cptpackrat/spacl-yaml/commit/6d3a9d560c491e0c1458adbef3529e4f97675aa5))

## [1.3.3](https://gitlab.com/cptpackrat/spacl-yaml/compare/v1.3.2...v1.3.3) (2020-01-27)


### Housekeeping

* Switched linter to standardx for better TS support. ([fa05ad4](https://gitlab.com/cptpackrat/spacl-yaml/commit/fa05ad4259f40571f322f2cefcc5d2bf649e25fb))
* Updated typescript target to ES2018. ([2fd6c8d](https://gitlab.com/cptpackrat/spacl-yaml/commit/2fd6c8d15a3e16244d58716169a900ed9c0beeb3))
* Changed @spacl/core to a peer dependency; relaxed version requirement. ([3855fe0](https://gitlab.com/cptpackrat/spacl-yaml/commit/3855fe061647acbf24246cc8320cb7f9a7011295))
* Updated dependencies. ([2250c7d](https://gitlab.com/cptpackrat/spacl-yaml/commit/2250c7df33e0fa85b78ed9507fc867169f88982b))

## [1.3.2](https://gitlab.com/cptpackrat/spacl-yaml/compare/v1.3.1...v1.3.2) (2020-01-01)


### Bug Fixes

* Removed redundant 'use strict' from source files. ([255f154](https://gitlab.com/cptpackrat/spacl-yaml/commit/255f154b595e5da44d92be7e1795629b65d3c901))


### Housekeeping

* Added '@spacl/core' types into typedoc output. ([fd8184e](https://gitlab.com/cptpackrat/spacl-yaml/commit/fd8184ec8f2669215e8adf03a6553c3bc157dedd))
* Added typedoc support with CI deployment to GitLab Pages. ([b504b81](https://gitlab.com/cptpackrat/spacl-yaml/commit/b504b81c60fb01f11a7cfb54b0ab8e4be1e70456))

## [1.3.1](https://gitlab.com/cptpackrat/spacl-yaml/compare/v1.3.0...v1.3.1) (2020-01-01)


### Housekeeping

* Updated dependencies. ([a277fd8](https://gitlab.com/cptpackrat/spacl-yaml/commit/a277fd8ffde963a9125b571cd7b9dfed0d82b0e6))

## [1.3.0](https://gitlab.com/cptpackrat/spacl-yaml/compare/v1.2.0...v1.3.0) (2019-10-20)


### Features

* Added 'stringify' function; converts a set of policies into YAML. ([b3fb278](https://gitlab.com/cptpackrat/spacl-yaml/commit/b3fb278))

## [1.2.0](https://gitlab.com/cptpackrat/spacl-yaml/compare/v1.1.2...v1.2.0) (2019-10-13)


### Features

* Bumped document version to '1.2' per changes in 459f0bc4 and 8dc53029; added logic for downgrading. ([becea7e](https://gitlab.com/cptpackrat/spacl-yaml/commit/becea7e))
* Support single strings for 'allow' and 'deny' as well as arrays. ([8dc5302](https://gitlab.com/cptpackrat/spacl-yaml/commit/8dc5302))
* Update @spacl/core dependency, use specification version 1.1 by default. ([459f0bc](https://gitlab.com/cptpackrat/spacl-yaml/commit/459f0bc))

## [1.1.2](https://gitlab.com/cptpackrat/spacl-yaml/compare/v1.1.1...v1.1.2) (2019-10-13)


### Bug Fixes

* Make dependency on @spacl/core stricter about versions. ([96ea4b6](https://gitlab.com/cptpackrat/spacl-yaml/commit/96ea4b6))

## [1.1.1](https://gitlab.com/cptpackrat/spacl-yaml/compare/v1.1.0...v1.1.1) (2019-10-02)


### Bug Fixes

* Forgot to increment document version per changes in 4387e973; bumped to '1.1', added logic for downgrading. ([7a64f2c](https://gitlab.com/cptpackrat/spacl-yaml/commit/7a64f2c))

## [1.1.0](https://gitlab.com/cptpackrat/spacl-yaml/compare/v1.0.0...v1.1.0) (2019-10-01)


### Features

* Multiple rules can now be created at once by using the 'paths' key to supply an array of path specs. ([4387e97](https://gitlab.com/cptpackrat/spacl-yaml/commit/4387e97))
* Rules governing the same path spec are now automatically merged during parsing. ([5ca9ecc](https://gitlab.com/cptpackrat/spacl-yaml/commit/5ca9ecc))
